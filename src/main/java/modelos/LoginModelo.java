package modelos;

public class LoginModelo {

    private String email;
    private String password;

    public LoginModelo(){
        this.email = "admin@gmail.com";
        this.password = "senha1";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
