package modelos;

public class EditandoTarefaPorParteModelo {

    private String id;
    private String title;
    private String description;
    private String deadline;
    private Boolean done;

    public EditandoTarefaPorParteModelo(){
        this.id = "557";
        this.title = "Tarefa de Criar Lista 2";
        this.description = "Teste tarefa editado por parte";
        this.deadline = "2020-08-16T08:00:00.000Z";
        this.done = true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
