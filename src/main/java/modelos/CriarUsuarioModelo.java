package modelos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CriarUsuarioModelo {

    private String email;
    private String password;
    private String passwordConfirmation;

    public CriarUsuarioModelo(){
        this.email = "admin@gmail.com";
        this.password = "senha1";
        this.passwordConfirmation = "senha1";

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @JsonProperty("password_confirmation")
    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }
    @JsonProperty("password_confirmation")
    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
