package modelos;

public class CriarListaModelo {

    private String title;
    private String description;
    private String deadline;
    private boolean done;

    public CriarListaModelo(){
        this.title = "tarefa de Criar Lista";
        this.description = "Teste tarefa";
        this.deadline = "2020-08-16  08:00";
        this.done = true;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
