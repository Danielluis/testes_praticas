package utils;

import java.io.*;
import java.util.Properties;

public class FileOperation {

    public static Properties getProperties(String filePath) throws IOException {

        InputStream inputStream = null;
        Properties properties = new Properties();

        try {

            File file = new File(filePath);
            inputStream = new FileInputStream(file);
            properties.load(inputStream);
            return properties;

        }catch (Exception e){

            e.printStackTrace();

        }finally {

            inputStream.close();

        }

        return properties;
    }

    public static void setProperties(String filePath, String key, String value) throws IOException {

        Properties properties = getProperties(filePath);


        try {

            OutputStream outputStream = new FileOutputStream(filePath);
            properties.setProperty(key,value);
            properties.store(outputStream,null);
        }catch (IOException e){
            e.printStackTrace();

        }
    }

}
