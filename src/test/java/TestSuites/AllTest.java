package TestSuites;

import Test_Cases.*;


import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;



@RunWith(JUnitPlatform.class)
@SelectClasses({postLogin.class,
                    postCriarListaDeTarefa.class,
                    getListarTarefas.class,
                    putEditarTarefas.class,
                    patchEditandoTarefaPorParte.class,
                    deleteDeletarTarefas.class})
public class AllTest {
}
