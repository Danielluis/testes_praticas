package Test_Cases;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.matcher.RestAssuredMatchers;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXParseException;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class postContatosXmlSchema {

    private static RequestSpecification requestSpecification;

    @BeforeEach
    public void inicializar(){
        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://restapi.wcaquino.me")
                .setBasePath("/usersXML")
                .build();

    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void criarContatoXmlValidaSchema(){
        given()
                .spec(requestSpecification)
        .when()
                .get()
        .then()
                .log()
                .body()
                .statusCode(200)
                .body(RestAssuredMatchers.matchesXsdInClasspath("Schemas/userAquino.xsd"));
    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void naoDeveValidarTesteXmlSchema(){
        SAXParseException saxParseException = assertThrows(SAXParseException.class,() -> {
            given()
                    .spec(requestSpecification)
            .when()
                    .get()
            .then()
                    .log()
                    .body()
                    .statusCode(200)
                    .body(RestAssuredMatchers.matchesXsdInClasspath("Schemas/userInvalideAquino.xsd"));
        });

        assertTrue(saxParseException.getMessage().contains("{last-name}"));
    }

}
