package Test_Cases;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import modelos.RequestContatosModelo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class postContatosModel {


    private static RequestSpecification requestSpecification;
    private static ResponseSpecification responseSpecification;
    private static RequestSpecification requestSpecDelete;
    private RequestContatosModelo requestContatosModelo = new RequestContatosModelo();



    @BeforeEach
    public void inicializar(){
        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("/contacts")
                .setBody(requestContatosModelo)
                .setContentType(ContentType.JSON)
                .addHeader("Content-Type","application/json")
                .addHeader("Accept","application/vnd.tasksmanager.v2")
                .build();

        requestSpecDelete = new RequestSpecBuilder()
                .addHeader("Content-Type","application/json")
                .addHeader("Accept","application/vnd.tasksmanager.v2")
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("/contacts")
                .setContentType(ContentType.JSON)
                .build();

        responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();

    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void criarContatos(){
        String id = given()
                .spec(requestSpecification)
        .when()
                .post()
        .then()
                .spec(responseSpecification)
                .and()
                .log()
                .body()
                .statusCode(201)
                .extract()
                .path("data.id");

        deletaContato(id);
        verificaSeDeletouPeloId(id);

    }

    private void verificaSeDeletouPeloId(String id){
        given()
                .spec(requestSpecDelete)
                .when()
                .get("/" + id)
                .then()
                .spec(responseSpecification)
                .log()
                .status()
                .statusCode(404);


    }

    private void deletaContato(String id){
        given()
                .spec(requestSpecification)
                .when()
                .delete("/" + id)
                .then()
                .log()
                .ifValidationFails(LogDetail.STATUS)
                .statusCode(204);
    }


}
