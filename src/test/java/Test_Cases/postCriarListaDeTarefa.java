package Test_Cases;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import modelos.CriarListaModelo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.FileOperation;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class postCriarListaDeTarefa {

    private static RequestSpecification requestSpecification;
    private static ResponseSpecification responseSpecification;


    @BeforeEach
    public void inicializar() throws IOException {
        Map<String, CriarListaModelo> criarListaModeloMap = new HashMap<>();
        criarListaModeloMap.put("task", new CriarListaModelo());

        String token = FileOperation.getProperties("src"+ File.separator +"main" + File.separator +"resources" + File.separator + "Properties" + File.separator + "token.properties").getProperty("token");

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("/tasks")
                .setBody(criarListaModeloMap)
                .setContentType(ContentType.JSON)
                .addHeader("Accept","applications")
                .addHeader("Authorization", token)
                .build();
        responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();
    }

    @Test
    public void criarLista() throws IOException {

        String id = given()
                .log()
                .all()
                .spec(requestSpecification)
        .when()
                .post()
        .then()
                .log()
                .body()
                .extract()
                .path("data.id");

        FileOperation.setProperties("src"+ File.separator +"main" + File.separator +"resources" + File.separator + "Properties" + File.separator + "token.properties", "id", id);
//        deletarTarefa(id);


    }

    public void deletarTarefa(String id){


        given()
                .spec(requestSpecification)
        .when()
                .delete("/" + id)
        .then()
                .log()
                .all();

    }


}
