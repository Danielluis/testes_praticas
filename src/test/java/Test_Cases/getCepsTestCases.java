package Test_Cases;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.FileOperation;

import java.io.File;
import java.io.IOException;

import static io.restassured.RestAssured.given;

public class getCepsTestCases {

    private static RequestSpecification requestSpecification;
    private static ResponseSpecification responseSpecification;
    private static String cep;



    @BeforeEach
    public void inicializar() throws IOException {

//        String caminho = System.getProperty("user.dir") + File.separator + "src" + File.separator
//                + "main" + File.separator + "resources" + File.separator + "Properties" + File.separator;

        cep = FileOperation.getProperties("src" + File.separator + "main" + File.separator + "resources" + File.separator +"Properties" + File.separator + "ceps.properties").getProperty("cep2");

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://www.zippopotam.us")
                .setBasePath("/us")
                .build();
        responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();
    }

    @Test
    public void ceps() throws IOException {

        String country =

        given()
                .spec(requestSpecification)
        .when()
                .get(cep)
        .then()
                .log()
                .body()
                .spec(responseSpecification)
                .extract()
                .path("country");

       FileOperation.setProperties("src" + File.separator + "main" + File.separator + "resources" + File.separator +"Properties" + File.separator + "ceps.properties", "country", country );

    }

    @Test
    public void ceps2() throws IOException{
        String state =
                given()
                    .spec(requestSpecification)
                .when()
                    .get(cep)
                .then()
                    .log()
                    .body()
                .spec(responseSpecification)
                .extract()
                .path("state");

        FileOperation.setProperties("src" + File.separator + "main" + File.separator + "resources" + File.separator +"Properties" + File.separator + "ceps.properties", "state", state );
    }




}
