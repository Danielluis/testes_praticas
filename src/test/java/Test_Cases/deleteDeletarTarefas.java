package Test_Cases;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import modelos.EditandoTarefaPorParteModelo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.FileOperation;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class deleteDeletarTarefas {

    private static RequestSpecification requestSpecification;
    private static ResponseSpecification responseSpecification;

    @BeforeEach
    public void inicializar() throws IOException {

        String token = FileOperation.getProperties("src" + File.separator + "main" + File.separator + "resources" + File.separator + "Properties" + File.separator + "token.properties").getProperty("token");

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("/tasks")
                .setContentType(ContentType.JSON)
                .addHeader("Accept","applications")
                .addHeader("Authorization",token)
                .build();
        responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();

    }
    @Test
        public void deletarTarefa(){
            given()
                    .log()
                    .all()
                    .spec(requestSpecification)
            .when()
                    .delete()
            .then()
                    .log()
                    .all();
    }

}
