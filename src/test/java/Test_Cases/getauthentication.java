package Test_Cases;

import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class getauthentication {

    private RequestSpecification requestSpecification;
    private ResponseSpecification responseSpecification;

    @Test
    public void authentication(){
        given()
        .when()
            .get("http://admin:senha@restapi.wcaquino.me/basicauth")
        .then()
            .log()
            .all();

    }

    @Test
    public void authentications2(){
        given()
                .log()
                .all()
                .auth()
                .preemptive()
                .basic("admin" , "senha")
        .when()
                .get("http://restapi.wcaquino.me/basicauth2")
        .then()
                .log()
                .all();

    }



}
