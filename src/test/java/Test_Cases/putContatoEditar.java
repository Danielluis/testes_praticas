package Test_Cases;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import modelos.RequestContatosModelo;
import modelos.RequestContatosModeloEditado;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class putContatoEditar {


    private static RequestSpecification requestSpecification;
    private static RequestSpecification requestSpecPut;
    private  static ResponseSpecification responseSpecification;
    private RequestContatosModeloEditado requestContatosModeloEditado = new RequestContatosModeloEditado();
    private RequestContatosModelo requestContatosModelo = new RequestContatosModelo();

    @BeforeEach
    public void inicializar(){
        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("contacts")
                .setBody(requestContatosModelo)
                .setContentType(ContentType.JSON)
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .build();

        requestSpecPut = new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("contacts")
                .setBody(requestContatosModeloEditado)
                .setContentType(ContentType.JSON)
                .addHeader("Content-Typer", "application/json")
                .addHeader("Accept", "application/json")
                .build();

        responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();


    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void criarContatos() {
        String id = given()
                .spec(requestSpecification)
        .when()
                .post()
        .then()
                .spec(responseSpecification)
                .and()
                .log()
                .body()
                .statusCode(201)
                .extract()
                .path("data.id");

        System.out.println("id: " + id);
        editarContatoPut(id);
        deletaContato(id);

        }


    private void editarContatoPut(String id){

        given()
                .spec(requestSpecPut)
        .when()
                .put("/" + id)
        .then()
                .log()
                .body()
                .and()
                .log()
                .ifValidationFails(LogDetail.STATUS)
                .statusCode(200)
                .and()
                .body("data.attributes.state", equalTo("Rio de Janeiro"));





    }

    private void deletaContato(String id){
        given()
                .spec(requestSpecification)
                .when()
                .delete("/" + id)
                .then()
                .log()
                .ifValidationFails(LogDetail.STATUS)
                .statusCode(204);

    }


    }
