package Test_Cases;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import modelos.CriarUsuarioModelo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class postCriaUsuario {

    private RequestSpecification requestSpecification;
    private ResponseSpecification responseSpecification;

    @BeforeEach
    public void inicializar(){

        Map<String, CriarUsuarioModelo> modeloMap = new HashMap<>();
        modeloMap.put("user", new CriarUsuarioModelo());

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("/users")
                .setBody(modeloMap)
                .setContentType(ContentType.JSON)
                .addHeader("Accerpt","application")
                .build();
        responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();
    }

    @Test
    public void criarUsuario(){
        given()
                .log()
                .all()
                .spec(requestSpecification)
        .when()
                .post()
        .then()
                .log()
                .all();


    }



}
