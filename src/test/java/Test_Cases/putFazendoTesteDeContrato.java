package Test_Cases;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.matcher.RestAssuredMatchers;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import modelos.RequestContatosModelo;
import modelos.RequestContatosModeloEditado;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class putFazendoTesteDeContrato {

    private static RequestSpecification requestSpecification;
    private static ResponseSpecification responseSpecification;
    private RequestSpecification requestSpecPut;
    private RequestContatosModelo requestContatosModelo = new RequestContatosModelo();
    private RequestContatosModeloEditado requestContatosModeloEditado = new RequestContatosModeloEditado() ;


    @BeforeEach
    public void inicializar(){

        requestSpecification = new RequestSpecBuilder()
              .setBaseUri("https://api-de-tarefas.herokuapp.com")
              .setBasePath("/contacts")
              .setBody(requestContatosModelo)
              .setContentType(ContentType.JSON)
              .addHeader("content-Typer","application/json")
              .addHeader("accept","application/json")
              .build();

        requestSpecPut = new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("/contacts")
                .setBody(requestContatosModeloEditado)
                .setContentType(ContentType.JSON)
                .addHeader("content-Typer","application/json")
                .addHeader("accept","application/json")
                .build();



      responseSpecification = new ResponseSpecBuilder()
              .expectContentType(ContentType.JSON)
              .build();

    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void putContatoComTestSchema(){
        String id = given()
                .spec(requestSpecification)
        .when()
                .post()
        .then()
                .spec(responseSpecification)
                .and()
                .log()
                .body()
                .statusCode(201)
                .extract()
                .path("data.id");

        editaContatoComPut(id);
        deletaContato(id);
    }
    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void editaContatoComPut(String id){

        given()
               .spec(requestSpecification)
        .when()
                .put("/" + id)
        .then()
                .log()
                .body()
                .and()
                .statusCode(200)
                .log()
                .body()
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("Schemas/criarContratoSchema.json"));

    }

    private void deletaContato(String id){
        given()
                .spec(requestSpecification)
                .when()
                .delete("/" + id)
                .then()
                .log()
                .ifValidationFails(LogDetail.STATUS)
                .statusCode(204);
    }

}
