package Test_Cases;

import org.json.JSONArray;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class jsonArray {

    @Test
    public void jsonTest(){

        String resposta = given()

                .when()
                        .get("https://viacep.com.br/ws/RS/Gravatai/Barroso/json/")
                .then()
                        .log()
                        .body()
                        .extract()
                        .response()
                        .getBody()
                        .asString();

        JSONArray jsonArray = new JSONArray(resposta);
        System.out.println(jsonArray.get(0));

    }

    @Test
    public void jsonTest2() {

        ArrayList<Map<String,String>> resposta = given()

                .when()
                .get("https://viacep.com.br/ws/RS/Gravatai/Barroso/json/")
                .then()
                .log()
                .body()
                .extract()
                .response()
                .getBody()
                .path("");

        System.out.println(resposta.get(0).get("logradouro"));

    }



}
