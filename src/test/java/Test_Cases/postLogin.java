package Test_Cases;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import modelos.LoginModelo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.FileOperation;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class postLogin {

    RequestSpecification requestSpecification;
    ResponseSpecification responseSpecification;

    @BeforeEach
    public void inicializar(){

        Map<String, LoginModelo> loginModeloMap = new HashMap<>();
        loginModeloMap.put("session", new LoginModelo());

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("/sessions")
                .setBody(loginModeloMap)
                .setContentType(ContentType.JSON)
                .build();
        responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();

    }

    @Test
    public void Login() throws IOException {
        String token =

        given()
                .spec(requestSpecification)
        .when()
                .post()
        .then()
                .log()
                .all()
                .extract()
                .path("data.attributes.auth-token");

        System.out.println(token);

        FileOperation.setProperties("src" + File.separator + "main" + File.separator + "resources" + File.separator +"Properties" + File.separator + "token.properties", "token", token );

    }
}
