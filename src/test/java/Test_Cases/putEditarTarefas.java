package Test_Cases;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import modelos.EditarTarefaModelo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.FileOperation;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class putEditarTarefas {

    RequestSpecification requestSpecification;
    ResponseSpecification responseSpecification;

    @BeforeEach
    public void inicializar() throws IOException {

        Map<String, EditarTarefaModelo> editarTarefaModeloMap = new HashMap<>();
        editarTarefaModeloMap.put("task", new EditarTarefaModelo());

        String token = FileOperation.getProperties("src"+ File.separator + "main" + File.separator + "resources" + File.separator + "Properties" + File.separator + "token.properties").getProperty("token");

        requestSpecification= new RequestSpecBuilder()
                .setBaseUri("https://api-de-tarefas.herokuapp.com")
                .setBasePath("/tasks")
                .setBody(editarTarefaModeloMap)
                .setContentType(ContentType.JSON)
                .addHeader("Accept","application")
                .addHeader("Authorization", token)
                .build();
        responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();


    }

    @Test
    public void editarTarefas(){
        given()
                .log()
                .all()
                .spec(requestSpecification)
        .when()
                .put()
        .then()
                .log()
                .body();
    }

}
