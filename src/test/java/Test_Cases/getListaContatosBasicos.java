package Test_Cases;

import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.restassured.filter.log.LogDetail;
import io.restassured.response.Response;
import io.qameta.allure.Description;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.hamcrest.Matchers.*;

import static io.restassured.RestAssured.given;

public class getListaContatosBasicos {

    @Description("Teste Allure")
    @DisplayName("Mostrando os Contatos da Lista")
    @Feature("Lista")
    @Issue("Link para bugTracker")
    @Test
    public void listaContatos(){
        String baseURI = "https://api-de-tarefas.herokuapp.com/contacts";

            given()

            .when()
                    .get(baseURI)
            .then()
                    .log()
                    .body()
                    .assertThat()
                    .statusCode(200);

    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void listaContatos2(){
        String baseURI = "https://api-de-tarefas.herokuapp.com/contacts";

        Response resposta = given()
                .when()
                      .get(baseURI)
                .then()
                    .log()
                    .body()
                    .assertThat()
                    .statusCode(200)
                    .extract().response();


        resposta.body().prettyPrint();

    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void listaContatos3(){
        String baseURI = "https://api-de-tarefas.herokuapp.com/contacts";

       String  resposta = given()
               .when()
                    .get(baseURI)
               .then()
                    .assertThat().statusCode(200)
                    .extract().path("data[3].attributes.name");
        System.out.println(resposta);


    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void listaContatos4(){
        String baseURI = "https://api-de-tarefas.herokuapp.com/contacts";

        String  resposta = given()
                .when()
                    .get(baseURI)
                .then()
                .log()
                .ifValidationFails(LogDetail.BODY).assertThat().statusCode(200).extract().path("data[0].id");

        System.out.println(resposta);
    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void listaContatos5(){

        String baseURI = "https://api-de-tarefas.herokuapp.com/contacts";
        String id = "1221";

                given()
                .when()
                   .get(baseURI + "/" + id)
                .then()
                    .log()
                    .body()
                    .assertThat()
                    .statusCode(200)
                    .body("data.id", equalTo(id));


    }

    @Description("")
    @DisplayName("")
    @Feature("")
    @Issue("")
    @Test
    public void listaContatos6() {

        String baseURI = "https://api-de-tarefas.herokuapp.com/contacts";


        given()
            .when()
                .get(baseURI)
            .then()
                .log()
                .body()
                .assertThat()
                .statusCode(200)
                .body("data.attributes[3].name", equalTo("Renan"));


    }

    }
